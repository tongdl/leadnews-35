package com.heima.green;

import com.heima.minio.MinIoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(exclude = MinIoConfiguration.class)
public class GreenApplication {
    public static void main(String[] args) {
        SpringApplication.run(GreenApplication.class, args);
    }
}

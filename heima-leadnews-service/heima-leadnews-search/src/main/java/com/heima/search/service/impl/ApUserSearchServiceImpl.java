package com.heima.search.service.impl;

import cn.hutool.core.util.StrUtil;
import com.heima.model.search.pojos.ApUserSearch;
import com.heima.search.service.ApUserSearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class ApUserSearchServiceImpl implements ApUserSearchService {

    // 0. 注入mongoTemplate;
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 保存用户搜索历史记录
     *
     * @param keyword
     * @param userId
     */
    @Override
    @Async
    public void insert(String keyword, Integer userId) {
        // 1. 入参判空
        if (StrUtil.isBlank(keyword) || userId == null) {
            return;
        }

        // 2. 查询当前用户的搜索关键词
        // 参数1：Query对象（查询条件）
        // 参数2：我要查询哪张表
        // 使用mongodb的Query对象来组装查询条件
        // 使用Query的addCriteria方法来添加具体的查询字段
        // addCriteria方法接收一个Criteria对象
        // 在 Criteria 对象里面使用 and方法设置查询的字段名，使用is来设置要查询的值
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and("keyword").is(keyword).and("userId").is(userId);
        query.addCriteria(criteria);
        ApUserSearch one = mongoTemplate.findOne(query, ApUserSearch.class);

        if (one != null) {
            // 3. 存在更新创建时间
            // 使用 updateFirst 方法更新数据，方法接收三个参数
            // 参数一：查询条件
            // 参数二：更新的数据
            // 使用 mongodb 的 Update 对象来组装查询条件
            // 参数三：更新的表
            Update update = new Update();
            update.set("createdTime", new Date());
            mongoTemplate.updateFirst(query, update, ApUserSearch.class);
            return;
        }


        // 4. 不存在，判断当前历史记录总数量是否超过10

        Query query2 = Query.query(Criteria.where("userId").is(userId));
        List<ApUserSearch> apUserSearches = mongoTemplate.find(query2, ApUserSearch.class);
        ApUserSearch userSearch = new ApUserSearch();
        userSearch.setCreatedTime(new Date());
        userSearch.setUserId(userId);
        userSearch.setKeyword(keyword);
        if (apUserSearches.size() < 10) {
            // 新增操作
            mongoTemplate.save(userSearch);
        } else {
            // 参数一：要替换的数据的查询条件
            // 参数二：要替换进去的数据
            ApUserSearch lastSearch = apUserSearches.get(apUserSearches.size() - 1);
            Query idQuery = Query.query(Criteria.where("id").is(lastSearch.getId()));
            mongoTemplate.findAndReplace(idQuery, userSearch);
        }
    }
}
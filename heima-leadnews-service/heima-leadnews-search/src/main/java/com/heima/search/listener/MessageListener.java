package com.heima.search.listener;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.alibaba.fastjson.JSON;
import com.heima.model.search.pojos.SearchArticleVo;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

// 1. 添加@Component注解，当项目启动时会自动加载
@Component
public class MessageListener {

    // 2. 注入 RestHighLevelClient
    @Autowired
    private RestHighLevelClient restHighLevelClient;

    // 3. @RabbitListener 注解，声明当前方法为消费者
    // queues 属性声明要监听的队列
    @RabbitListener(queues = "es.queue")
    public void getMessage(String message) throws IOException {

        // 4. 判断消息是否为空
        if(StringUtils.isBlank(message)){
            return;
        }

        // 5. 创建索引
        // 将消息转为SearchArticleVo
        SearchArticleVo vo = JSON.parseObject(message, SearchArticleVo.class);

        // 5.1 创建IndexRequest的时候，需要指定一下插入数据的表（映射）叫什么名字 -> app_info_article
        IndexRequest indexRequest = new IndexRequest("app_info_article");
        // 5.2 要借助indexRequest对象，去设置要写入的数据
        // id() 设置我即将写入的这条数据的主键
        // source() 设置我即将写入的数据(带两个参数: 字符串, 数据类型（JSON）)
        Long id = vo.getId();
        indexRequest.id(id.toString());
        indexRequest.source(message, XContentType.JSON);

        // 5.3 调用 RestHighLevelClient的index方法实现往es里面写数据
        // 参数1：IndexRequest对象，用来设置写入的数据
        // 参数2：RequestOption对象，用来设置写入类型
        IndexResponse index = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);

        System.out.println("成功写入一条数据，id为:" + index.getId());
        System.out.println(index.getIndex() + ":" + vo.getId());
    }

}

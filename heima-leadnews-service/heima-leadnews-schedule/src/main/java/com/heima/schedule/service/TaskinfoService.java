package com.heima.schedule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.schedule.pojos.Taskinfo;

public interface TaskinfoService extends IService<Taskinfo> {
}

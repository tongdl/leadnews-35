package com.heima.schedule.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.schedule.pojos.TaskinfoLogs;
import com.heima.schedule.mapper.TaskinfoLogsMapper;
import com.heima.schedule.service.TaskinfoLogsService;
import org.springframework.stereotype.Service;

@Service
public class TaskinfoLogsServiceImpl extends ServiceImpl<TaskinfoLogsMapper, TaskinfoLogs> implements TaskinfoLogsService {
}
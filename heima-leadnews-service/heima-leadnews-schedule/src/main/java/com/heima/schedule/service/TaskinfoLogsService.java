package com.heima.schedule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.schedule.pojos.TaskinfoLogs;

public interface TaskinfoLogsService extends IService<TaskinfoLogs> {
}

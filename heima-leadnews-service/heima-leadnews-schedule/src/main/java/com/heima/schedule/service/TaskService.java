package com.heima.schedule.service;

import com.heima.model.schedule.dto.Task;

public interface TaskService {
    /**
     * 添加任务
     *
     * @param task 任务对象
     * @return 任务id
     */
    Long addTask(Task task);

    /**
     * 取消任务
     *
     * @param taskId 任务id
     * @return 取消结果
     */
    Boolean cancelTask(Long taskId);

    /**
     * 按照类型和优先级来拉取任务
     *
     * @return Task
     */
    Task pollTask();
}
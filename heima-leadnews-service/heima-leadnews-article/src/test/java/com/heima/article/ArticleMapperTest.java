package com.heima.article;

import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.HotArticleService;
import com.heima.model.article.pojos.ApArticle;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
public class ArticleMapperTest {

    @Autowired
    private ApArticleMapper apArticleMapper;

    @Autowired
    private HotArticleService hotArticleService;

    @Test
    public void test(){
        // 参数：5天前的日期
        //LocalDateTime date = LocalDateTime.now().minusDays(5);
        LocalDateTime of = LocalDateTime.of(2021, 4, 20, 0, 0, 0).minusDays(5);
        List<ApArticle> articleListByLast5days = apArticleMapper.findArticleListByLast5days(of);

        for (ApArticle articleListByLast5day : articleListByLast5days) {
            System.out.println(articleListByLast5day.getTitle());
            System.out.println(articleListByLast5day.getPublishTime());
        }
    }

    @Test
    public void test2() {

        hotArticleService.computeHotArticle();

    }
}

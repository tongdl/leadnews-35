package com.heima.article;

import com.alibaba.fastjson.JSON;
import com.heima.article.service.ApArticleService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class ArticleTest {
    @Autowired
    private ApArticleService apArticleService;

    @Test
    public void testSaveArticle() {
        ArticleDto dto = new ArticleDto();
        dto.setContent("[{\"type\":\"text\",\"value\":\"Hutool是一个小而全的Java工具类库，通过静态方法封装，降低相关API的学习成本，提高工作效率，使Java拥有函数式语言般的优雅，让Java语言也可以“甜甜的”。\"},{\"type\":\"image\",\"value\":\"http://192.168.200.130/group1/M00/00/00/wKjIgl892vuAXr_MAASCMYD0yzc919.jpg\"},{\"type\":\"text\",\"value\":\"We are checking your browser... mvnrepository.com\"}]");
        dto.setAuthorId((long) 1102);
        dto.setAuthorName("admin");
        dto.setChannelId(2);
        dto.setChannelName("mysql");
        dto.setImages("http://192.168.200.130:9000/leadnews/2021/04/26/5ddbdb5c68094ce393b08a47860da275.jpg");
        dto.setTitle("黑马头条项目背景22222222222222");
        dto.setCreatedTime(new Date());
        dto.setPublishTime(new Date());
        dto.setLabels("黑马头条");
        dto.setLayout((short) 1);

        ResponseResult responseResult = apArticleService.saveArticle(dto);
        System.out.println(JSON.toJSONString(responseResult));
    }

    @Test
    public void testCreateArticleESIndex() {
        String content = "[{\"type\":\"text\",\"value\":\"Hutool是一个小而全的Java工具类库，通过静态方法封装，降低相关API的学习成本，提高工作效率，使Java拥有函数式语言般的优雅，让Java语言也可以“甜甜的”。\"},{\"type\":\"image\",\"value\":\"http://192.168.200.130/group1/M00/00/00/wKjIgl892vuAXr_MAASCMYD0yzc919.jpg\"},{\"type\":\"text\",\"value\":\"We are checking your browser... mvnrepository.com\"}]";

        ApArticle apArticle = new ApArticle();
        apArticle.setId(1000022L);
        apArticle.setAuthorId((long) 1102);
        apArticle.setAuthorName("admin");
        apArticle.setChannelId(2);
        apArticle.setChannelName("mysql");
        apArticle.setImages("http://192.168.200.130:9000/leadnews/2021/04/26/5ddbdb5c68094ce393b08a47860da275.jpg");
        apArticle.setTitle("黑马头条项目背景22222222222222");
        apArticle.setCreatedTime(new Date());
        apArticle.setPublishTime(new Date());
        apArticle.setLabels("黑马头条");
        apArticle.setLayout((short) 1);

        apArticleService.createArticleESIndex(apArticle, content);
    }
}
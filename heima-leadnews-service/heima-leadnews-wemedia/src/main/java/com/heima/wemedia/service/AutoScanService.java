package com.heima.wemedia.service;

import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.wemedia.pojos.WmNews;

import java.util.List;
import java.util.Map;

public interface AutoScanService {
    /**
     * 自媒体文章审核
     *
     * @param newsId 自媒体文章id
     */
    void autoScanWmNews(Integer newsId) throws Exception;

    /**
     * 提取新闻文字内容
     *
     * @param title   标题
     * @param content 内容
     * @return 组合后的字符串
     */
    String getText(String title, String content);

    /**
     * 提取图片
     * 因为后续阿里云图片审核需要List<byte[]>结构，所以这里出参是这个类型
     *
     * @param images  封面图
     * @param content 内容
     * @return 图片byte集合
     */
    List<byte[]> getImages(String images, String content);

    /**
     * 更新新闻状态
     *
     * @param status    状态
     * @param reason    审核结果
     * @param articleId 文章id
     * @param newsId    新闻id（查询条件）
     * @return 是否成功
     */
    Boolean updateWmNews(Integer status, String reason, Long articleId, Integer newsId);

    /**
     * 处理阿里云审核结果
     *
     * @param result 审核结果
     * @param newsId 新闻id
     * @return 是否通过
     */
    Boolean checkResult(Map result, Integer newsId);

    /**
     * 将WmNews转为ArticleDto结构
     *
     * @param wmNews wmNews
     * @return ArticleDto
     */
    ArticleDto wmNews2ArticleDto(WmNews wmNews);

    /**
     * DFA算法检测敏感词
     *
     * @param text 待检测的文字
     * @return 是否通过检测（true为通过，false为不通过）
     */
    Boolean dfaCheck(String text);

    /**
     * 消费延迟队列数据
     */
    void getTask();
}
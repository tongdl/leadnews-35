package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.minio.MinIoTemplate;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.utils.thread.WmThreadLocalUtil;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.service.WmMaterialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
@Transactional
public class WmMaterialServiceImpl extends ServiceImpl<WmMaterialMapper, WmMaterial> implements WmMaterialService {

    // 0. 注入MinIO，配置MinIO
    @Autowired
    private MinIoTemplate minIoTemplate;

    @Autowired
    private WmMaterialMapper wmMaterialMapper;

    @Override
    public ResponseResult upload(MultipartFile file) throws IOException {
        // 1. 判断文件对象是否为空
        if (file == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        // 2. 获取文件后缀名
        // 使用getContentType获取文件后缀名，获取到的文件后缀名 image/png -> ["image", "png"]
        String contentType = file.getContentType();
        String prefix = contentType.split("/")[1];

        // 3. 生成随机文件名
        // 可以使用 System.currentTimeMillis() 来生成随机不重复的文件名
        String fileName = System.currentTimeMillis() + "";

        // 4. 调用Minio方法实现上传，获取上传后网址
        // 参数一：后缀名
        // 参数二：文件名 名字+.png
        // 参数三：文件流
        // 参数四：文件类型 image
        String url = minIoTemplate.uploadFile(prefix, fileName + "." + prefix, file.getInputStream(), "image");

        // 5. 组装数据，插入MySQL，需要引入 wmMaterialMapper
        WmMaterial wmMaterial = new WmMaterial();
        wmMaterial.setUrl(url);
        wmMaterial.setType((short) 0);
        wmMaterial.setIsCollection((short) 0);
        wmMaterial.setCreatedTime(new Date());
        // 获取当前登录用户的id -> token
        wmMaterial.setUserId(WmThreadLocalUtil.getUser().getId());
        int insertMaterialResult = wmMaterialMapper.insert(wmMaterial);
        if (insertMaterialResult != 1) {
            return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
        }

        // 6. 将插入后素材信息返回给前端
        return ResponseResult.okResult(wmMaterial);
    }

    @Override
    public ResponseResult findList(WmMaterialDto dto) {
        // 1. 判断dto是否为空
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        // 2. 从dto中提取页码，每页显示条数，是否收藏
        Integer page = dto.getPage();
        Integer size = dto.getSize();
        Short isCollection = dto.getIsCollection();

        // 3. 构造查询条件（当前用户、是否收藏）
        LambdaQueryWrapper<WmMaterial> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(WmMaterial::getUserId, WmThreadLocalUtil.getUser().getId());
        wrapper.eq(WmMaterial::getIsCollection, isCollection);

        // 3.1 构建排序条件
        wrapper.orderByDesc(WmMaterial::getCreatedTime);

        // 3.2 分页查询
        Page<WmMaterial> page1 = new Page<>(page, size);
        Page<WmMaterial> pageResult = wmMaterialMapper.selectPage(page1, wrapper);

        List<WmMaterial> records = pageResult.getRecords();
        Long total = pageResult.getTotal();

        // 4. 构建返回结果，返回PageResponseResult对象
        // 数据集合
        // 每页显示几条，当前页面，总的条数
        PageResponseResult pageResponseResult = new PageResponseResult(page, size, total.intValue());
        pageResponseResult.setData(records);

        return pageResponseResult;
    }
}
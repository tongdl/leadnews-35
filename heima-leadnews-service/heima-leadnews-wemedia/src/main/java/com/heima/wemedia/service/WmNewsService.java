package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmNews;

import java.util.List;

public interface WmNewsService extends IService<WmNews> {
    /**
     * 查询文章
     *
     * @param dto dto
     * @return ResponseResult
     */
    ResponseResult findAll(WmNewsPageReqDto dto);

    /**
     * 新闻发布
     * @param dto dto
     * @return ResponseResult
     */
    ResponseResult submit(WmNewsDto dto);

    /**
     * 解析dto数据，转为实体类字段
     *
     * @param wmNewsDto wmNewsDto
     * @return WmNews
     */
    WmNews wmNewsDtoToWmNews(WmNewsDto wmNewsDto);

    /**
     * 提取内容字符串中的图片地址集合
     *
     * @param content 内容字符串
     * @return List<String> 图片地址集合
     */
    List<String> getContentImages(String content);

    /**
     * 添加素材 + 新闻素材关系
     * @param images 要添加的图片地址
     * @param type 0:内容图, 1:封面图
     * @param newsId 新闻id
     */
    Boolean addMaterial(List<String> images, Integer type, Integer newsId);

    /**
     * 从内容图获取图片，填充到封面图上
     *
     * @param type          布局方式
     * @param coverImage    前端传过来的封面图集合
     * @param contentImages 内容图集合
     * @return List<String> 封面图集合
     */
    List<String> autoLayout(short type, List<String> coverImage, List<String> contentImages);

    Boolean addToTask(Integer newsId, Long executeTime);
}
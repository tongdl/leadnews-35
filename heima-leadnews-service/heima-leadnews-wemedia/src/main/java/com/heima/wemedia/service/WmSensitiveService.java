package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.wemedia.pojos.WmSensitive;

/**
 * @author itheima
 * @since 2023-06-20
 */
public interface WmSensitiveService extends IService<WmSensitive> {
}

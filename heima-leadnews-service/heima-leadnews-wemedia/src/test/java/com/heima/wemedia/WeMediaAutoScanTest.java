package com.heima.wemedia;

import com.heima.utils.OcrUtil;
import com.heima.wemedia.service.AutoScanService;
import net.sourceforge.tess4j.TesseractException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

@SpringBootTest
public class WeMediaAutoScanTest {

    @Autowired
    private AutoScanService autoScanService;

    @Test
    public void testGetText() {
        String title = "黑马头条项目背景22222222222222";
        String content = "[{\"type\":\"text\",\"value\":\"Hutool是一个小而全的Java工具类库，通过静态方法封装，降低相关API的学习成本，提高工作效率，使Java拥有函数式语言般的优雅，让Java语言也可以“甜甜的”。\"},{\"type\":\"image\",\"value\":\"http://192.168.200.130/group1/M00/00/00/wKjIgl892vuAXr_MAASCMYD0yzc919.jpg\"},{\"type\":\"text\",\"value\":\"We are checking your browser... mvnrepository.com\"}]";
        String text = autoScanService.getText(title, content);
        System.out.println(text);
    }

    @Test
    public void testGetImage() {
        String image = "http://120.48.94.74:9000/leadnews/2023/06/19/666.jpg";
        String content = "[{\"type\":\"text\",\"value\":\"Hutool是一个小而全的Java工具类库，通过静态方法封装，降低相关API的学习成本，提高工作效率，使Java拥有函数式语言般的优雅，让Java语言也可以“甜甜的”。\"},{\"type\":\"image\",\"value\":\"http://120.48.94.74:9000/leadnews/2023/06/19/666.jpg\"},{\"type\":\"text\",\"value\":\"We are checking your browser... mvnrepository.com\"}]";
        List<byte[]> images = autoScanService.getImages(image, content);
        System.out.println(images);
    }

    @Test
    public void testDfa() {
        String str = "冰有害健康。";
        Boolean aBoolean = autoScanService.dfaCheck(str);
        System.out.println(aBoolean);
    }

    @Test
    public void testOcr() throws IOException, TesseractException {
        FileInputStream fis = new FileInputStream("C:\\space-35\\资料\\黑马头条\\day04\\image-20210524161243572.png");
        byte[] bytes = new byte[fis.available()];
        fis.read(bytes);

        ByteArrayInputStream image = new ByteArrayInputStream(bytes);
        BufferedImage read = ImageIO.read(image);
        String s = OcrUtil.doOcr(read);
        System.out.println(s);
    }

}
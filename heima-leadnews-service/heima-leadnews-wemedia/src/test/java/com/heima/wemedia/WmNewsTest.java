package com.heima.wemedia;

import cn.hutool.core.collection.CollectionUtil;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.thread.WmThreadLocalUtil;
import com.heima.wemedia.service.WmNewsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author itheima
 * @since 2023-06-16
 */
@SpringBootTest
public class WmNewsTest {

    @Autowired
    private WmNewsService wmNewsService;

    @Test
    public void testGetContentImages() {
        String content = "[{\"type\":\"image\",\"value\":\"http://heima-fastdfs.itheima.net/group1/M00/00/0C/rBEo4WOyRC6AYDTTAAArIxljsTE766.png\"},{\"type\":\"text\",\"value\":\"第一行内容\"},{\"type\":\"image\",\"value\":\"http://heima-fastdfs.itheima.net/group1/M00/00/0C/rBEo4WOyRByAUcGfAAAhaZ4d9Yg756.png\"},{\"type\":\"text\",\"value\":\"第2行内容\"},{\"type\":\"image\",\"value\":\"http://heima-fastdfs.itheima.net/group1/M00/00/0C/rBEo4WOyQ8uAcVh3AAETz_at48E839.png\"},{\"type\":\"text\",\"value\":\"第3行内容\"}]";
        List<String> contentImages = wmNewsService.getContentImages(content);
        for (String contentImage : contentImages) {
            System.out.println(contentImage);
        }
    }

    @Test
    public void testAddMaterial() {
        WmUser wmUser = new WmUser();
        wmUser.setId(1102);
        WmThreadLocalUtil.setUser(wmUser);

        List<String> images = CollectionUtil.newArrayList("http://1.png","http://2.png","http://3.png");
        Boolean aBoolean = wmNewsService.addMaterial(images, 0, 1);
        System.out.println("执行结果:" + aBoolean);
    }
}

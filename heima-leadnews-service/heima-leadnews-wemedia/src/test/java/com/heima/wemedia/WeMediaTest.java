package com.heima.wemedia;

import com.heima.minio.MinIoTemplate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest
public class WeMediaTest {

    @Autowired
    private MinIoTemplate minIoTemplate;

    @Test
    public void test() throws FileNotFoundException {
        // 1. 读取本地的一个文件
        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\32803\\Pictures\\379-1FH0154333.jpg");
        // 2. 调用minio上传方法
        // 参数一：文件后缀
        // 参数二：文件名
        // 参数三：文件内容
        // 参数四：文件类型 image
        String filePath = minIoTemplate.uploadFile("", "666.jpg", fileInputStream, "image");
        System.out.println(filePath);
    }


}

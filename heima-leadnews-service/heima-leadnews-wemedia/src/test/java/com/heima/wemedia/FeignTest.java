package com.heima.wemedia;

import com.alibaba.fastjson.JSON;
import com.heima.apis.IArticleClient;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class FeignTest {

    @Autowired
    private IArticleClient iArticleClient;

    @Test
    public void test() {
        ArticleDto dto = new ArticleDto();
        dto.setContent("[{\"type\":\"text\",\"value\":\"Hutool是一个小而全的Java工具类库，通过静态方法封装，降低相关API的学习成本，提高工作效率，使Java拥有函数式语言般的优雅，让Java语言也可以“甜甜的”。\"},{\"type\":\"image\",\"value\":\"http://192.168.200.130/group1/M00/00/00/wKjIgl892vuAXr_MAASCMYD0yzc919.jpg\"},{\"type\":\"text\",\"value\":\"We are checking your browser... mvnrepository.com\"}]");
        dto.setAuthorId((long) 1102);
        dto.setAuthorName("admin");
        dto.setChannelId(2);
        dto.setChannelName("mysql");
        dto.setImages("http://192.168.200.130:9000/leadnews/2021/04/26/5ddbdb5c68094ce393b08a47860da275.jpg");
        dto.setTitle("黑马头条项目背景22222222222222");
        dto.setCreatedTime(new Date());
        dto.setPublishTime(new Date());
        dto.setLabels("黑马头条");
        dto.setLayout((short) 1);
        ResponseResult responseResult = iArticleClient.saveArticle(dto);
        System.out.println(JSON.toJSONString(responseResult));
    }
}

package com.heima.apis;

import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

// 1. 使用注解@FeignClient, 声明接口所属的服务
@FeignClient("leadnews-article")
public interface IArticleClient {
    
    // 2. 注解声明接口访问路径和请求方式
    @PostMapping("/api/v1/article/save")
    ResponseResult saveArticle(@RequestBody ArticleDto dto);
    
}
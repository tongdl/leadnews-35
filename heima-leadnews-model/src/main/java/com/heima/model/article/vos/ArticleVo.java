package com.heima.model.article.vos;

import lombok.Data;

import java.util.Date;

/**
 * @author itheima
 * @since 2022-12-31
 */
@Data
public class ArticleVo {
    private Long id;
    private String title;
    private Long authorId;
    private String authorName;
    private Long channelId;
    private String channelName;
    private Integer layout;
    private String images;
    private Date createdTime;
    private Date publishTime;
    private Boolean syncStatus;
    private Boolean origin;
}

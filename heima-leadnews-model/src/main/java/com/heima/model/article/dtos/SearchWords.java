package com.heima.model.article.dtos;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author itheima
 * @since 2023-01-10
 */
@Data
@Document
public class SearchWords {
    private String id;
    private String userId;
    private String keyword;
    private Date createdTime;
}

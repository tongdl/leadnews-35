package com.heima.model.behavoir.dtos;

import lombok.Data;

/**
 * @author itheima
 * @since 2023-01-12
 */
@Data
public class LikeDto {
    private Long articleId;
    private Integer operation;
    private Integer type;
}

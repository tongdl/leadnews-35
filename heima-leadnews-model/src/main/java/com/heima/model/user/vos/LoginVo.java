package com.heima.model.user.vos;

import com.heima.model.user.pojos.ApUser;
import lombok.Data;

@Data
public class LoginVo {
    private ApUser user;
    private String token;
}

package com.heima.model.user.vos;

import lombok.Data;

import java.util.Date;

/**
 * @author itheima
 * @since 2022-12-28
 */
@Data
public class UserBaseVo {
    private Long id;
    private String name;
    private String phone;
    private Boolean sex;
    private Boolean status;
    private Integer flag;
    private Date createdTime;
}

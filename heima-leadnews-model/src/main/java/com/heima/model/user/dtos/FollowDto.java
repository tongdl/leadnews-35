package com.heima.model.user.dtos;

import lombok.Data;

/**
 * @author itheima
 * @since 2023-01-12
 */
@Data
public class FollowDto {
    private Long articleId;
    private Long authorId;
    private Integer operation;
}

package com.heima.model.common.vos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author itheima
 * @since 2022-12-28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    private String host;
    private Integer code;
    private String errorMessage;
    private Object data;

    public static Result success(Object data) {
        return new Result(null, 200, "成功", data);
    }

    public static Result fail(String errorMessage) {
        return new Result(null, 500, errorMessage, null);
    }
}
